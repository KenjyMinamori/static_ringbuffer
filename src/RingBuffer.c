/*
 ============================================================================
 Name        : RingBuffer.c
 Author      : Mikhail Natalenko
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "ringbuffer/ringbuffer.h"



#define BUFFER_LEN 4
#define BUFFER_TYPE uint8_t
#define ELEMENT_SIZE sizeof(BUFFER_TYPE)
#define BUFFER_SIZE ELEMENT_SIZE * BUFFER_LEN


typedef struct {
	uint32_t alpha;
	uint16_t beta;
	char gamma[3];
}my_struct_t;

uint8_t buffer[BUFFER_SIZE] = { 0 };

ring_buffer rb;
ring_buffer *ptr_rb = &rb;

void print_array(BUFFER_TYPE * array, size_t size)
{
	puts("ARR: ");
	for (int i= 0 ; i< size; i++)
	{
		printf ("%02d, ", array[i]);
	}
	puts("\n");
}

void print_rbuf(ring_buffer *rb) {
	puts("RBUF: ");
	for (int i = 0; i < rb->maxlen; i++) {
		BUFFER_TYPE * data = &rb->buffer[i * rb->element_size];
		//printf("{%02d, %02d, %3.3s}, ", data->alpha, data->beta, data->gamma);
		printf("%02d, ", *data );
	}
	puts("\n      ");
	for (int i = 0; i < rb->maxlen; i++) {
		if ((rb->head == rb->tail) && (rb->head == i)) {
			printf(" =, ");
		} else if (rb->head == i) {
			printf(" <, ");
		} else if (rb->tail == i) {
			printf("> , ");
		} else {
			printf("  , ");
		}
	}
	puts("\n");
}

void smart_push(void *i) {

	rbuf_status ret = ring_buffer_push(ptr_rb, i);
	switch (ret) {
	case RBUF_SUCCESS:
		print_rbuf(ptr_rb);
		printf("Write success: %d\n", *(BUFFER_TYPE*) i);
		break;
	case RBUF_FULL:
		printf("Write fail. Overflow: %d\n", *(BUFFER_TYPE*) i);
		break;
	default:
		printf("Write fail. some other error \n");
	}
	puts("-----------------------------------");

}

void smart_push_array(void *i, size_t s) {

	size_t ret = ring_buffer_push_array(ptr_rb, i, s);
	print_rbuf(ptr_rb);
	print_array(i, s);
	if (ret == s) {
		printf("Write array success size %d:\n", s);
	} else {
		printf("Write fail. Overflow: %d size:\n", s);
	}
	puts("-----------------------------------");

}

void smart_pop_array(void *i, size_t s) {

	size_t ret = ring_buffer_pop_array(ptr_rb, i, s);
	print_rbuf(ptr_rb);
	print_array(i, s);
	if (ret == s) {
		printf("Read array success size %d:\n", s);
	} else {
		printf("Read fail. Overflow: %d size:\n", s);
	}
	puts("-----------------------------------");

}
void smart_pop(void *i) {

	rbuf_status ret = ring_buffer_pop(ptr_rb, i);
	print_rbuf(ptr_rb);
	switch (ret) {
	case RBUF_SUCCESS:
		printf("READ success: %d\n", *(BUFFER_TYPE*) i);
		break;
	case RBUF_EMPTY:
		printf("READ fail. Overflow: %d\n", *(BUFFER_TYPE*) i);
		break;
	default:
		printf("READ fail. some other error \n");
	}
	puts("-----------------------------------");

}

void setup_rand() {
	static time_t t;
	srand((unsigned) time(&t));
}

int main(void) {
	setup_rand();
	printf("!!!Hello World!!! \n"); /* prints !!!Hello World!!! */

	ring_buffer_init(ptr_rb, buffer, BUFFER_LEN, ELEMENT_SIZE);

	print_rbuf(ptr_rb);
	//BUFFER_TYPE arr[] = {{12,13,"ab"}, {14,15,"cd"}, {16,17,"ef"}, {18,19,"gh"} };
	uint8_t arr[] = {1,2,3,4};
	smart_push_array(arr, 4);


	BUFFER_TYPE arr3[4] = { 0 };
	smart_pop_array(arr3, 3);

	//BUFFER_TYPE arr2[] = {{21,22,"jk"}, {23,24,"lm"}, {25,26,"no"}, {27,28,"pr"} };
	uint8_t arr2[] = {5,6,7,8};
	smart_push_array(arr2, 4);
	arr3[0] = 0;
	arr3[1] = 0;
	arr3[2] = 0;
	arr3[3] = 0;

	smart_pop_array(arr3, 3);
	arr3[0] = 0;
	arr3[1] = 0;
	arr3[2] = 0;
	arr3[3] = 0;
	smart_pop_array(arr3, 3);
//	for (BUFFER_TYPE i = 0; i < 10; i++) {
//		smart_push(&i);
//	}
//
//	for (BUFFER_TYPE i = 0; i < 10; i++) {
//		BUFFER_TYPE a = 0;
//		smart_pop(&a);
//	}

	for (int i = 0; i < 100; i++) {
		int a = rand() % 2;
		if (a) {
			BUFFER_TYPE random_int = {0};
			//random_int.alpha = rand() % 100;
			random_int = rand() % 100;
			smart_push(&random_int);
		} else {
			BUFFER_TYPE read = {0};
			smart_pop(&read);
		}
	}

	return EXIT_SUCCESS;
}
