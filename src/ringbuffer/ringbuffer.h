#ifndef GSM_RINGBUFFER_H_
#define GSM_RINGBUFFER_H_

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
  {
#endif

typedef struct
{
  uint8_t * buffer;
  uint32_t head;
  uint32_t tail;
  size_t element_size;
  int maxlen;
} ring_buffer;

typedef enum {RBUF_EMPTY = -3, RBUF_ASSERT = -2, RBUF_FULL = -1, RBUF_SUCCESS = 0} rbuf_status;

//Push data to ring buffer
rbuf_status
ring_buffer_push (ring_buffer *c, void * data);

//Pop data from buffer
rbuf_status
ring_buffer_pop (ring_buffer *c, void * data);

/** Pop array of data from buffer
 *  @param c buffer to read from
 *  @param buffer buffer
 *  @param size of elements to read
 *  @return amount of popped elements (could be less than size)
 */
size_t
ring_buffer_pop_array (ring_buffer *c, void * buffer, size_t size);

/** Push array of data to buffer
 *  @param c buffer to write to
 *  @param buffer buffer
 *  @param  size of elements to write
 *  @return amount of pushed elements (could be less than size)
 */
size_t
ring_buffer_push_array (ring_buffer *c, uint8_t * buffer, size_t size);

/**
 * Init buffer
 * @param c : Ringbuffer to init
 * @param buffer : buffer for rbuf
 * @param size
 * @return
 */
rbuf_status
ring_buffer_init (ring_buffer *c, uint8_t * buffer, size_t size, size_t element_size);

/** How much elements is available
 *  @param c: pointer to buffer
 *  @return amount of available elements
 */
size_t
ring_buffer_available (ring_buffer *c);

/** Move tail to head (empty the buffer)
 *  @param c: pointer to buffer
 *  @return result
 */
rbuf_status
ring_buffer_flush (ring_buffer *c);

/** Return amount of empty space
 *  @param c: pointer to buffer
 *  @return available capacity
 */
size_t
ring_buffer_get_capacity (ring_buffer *c);

/** Return limit of empty space
 *  @param c: pointer to buffer
 *  @return max capacity
 */
size_t
ring_buffer_get_size (ring_buffer *c);
#ifdef __cplusplus
}
#endif
#endif /* GSM_RINGBUFFER_H_ */
