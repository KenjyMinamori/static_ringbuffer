#include "ringbuffer.h"
#include <string.h>

void *
get_ptr(ring_buffer * c, size_t index)
{
	//TODO: check overflow
	return &c->buffer[index * c->element_size];
}

rbuf_status
ring_buffer_init (ring_buffer *c, uint8_t * buffer, size_t size, size_t element_size)
{
  if (!c)
    return RBUF_ASSERT;

  c->buffer = buffer;
  c->head = 0;
  c->tail = 0;
  c->maxlen = size;
  c->element_size = element_size;
  return RBUF_SUCCESS;
}

rbuf_status
ring_buffer_push (ring_buffer *c, void * data)
{
  if (!c)
    return RBUF_ASSERT;
  int next;

  next = c->head + 1;  // next is where head will point to after this write.
  if (next >= c->maxlen)
    next = 0;

  if (next == c->tail)  // if the head + 1 == tail, circular buffer is full
    return RBUF_FULL;

  memcpy(get_ptr(c, c->head), data,  c->element_size); // Read data and then move

  c->head = next;             // head to next data offset.
  return 0;  // return success to indicate successful push.
}

rbuf_status
ring_buffer_pop (ring_buffer *c, void *data)
{
  if (!c || !data)
    return RBUF_ASSERT;

  int next;

  if (c->head == c->tail)  // if the head == tail, we don't have any data
    return RBUF_EMPTY;

  next = c->tail + 1;  // next is where tail will point to after this read.
  if (next >= c->maxlen)
    next = 0;

  void * source = get_ptr(c, c->tail);
  memcpy(data, source , c->element_size); // Read data and then move

  c->tail = next;              // tail to next offset.
  return RBUF_SUCCESS;  // return success to indicate successful push.
}

size_t
ring_buffer_pop_array (ring_buffer *c, void * buffer, size_t size)
{
  size_t available = ring_buffer_available(c);
  if (size > available)
    size = available;

  if (size == 0) 
    return size;

  uint32_t next = c->tail + size;  // next is where tail will point to after this read.

  // Check overflow for the head
  if (next >= c->maxlen )
   next -= c->maxlen;

  /**
   * c->head - is a start point and next - is an end pont. If next bigger than c->head - we
   * can write incoming buffer dirrectly to ringbuffer (without overflow). Otherwise we should
   * split writing to two parts: from head to maxlen (phisical end of rbuf) and from zero to next  
   */
  if (next > c->tail)
    {
      // Just write buffer as it is.
      memcpy(buffer, get_ptr(c, c->tail), size* c->element_size);
      c->tail = next;
      return size;
    } 
  else 
    {
      // Split writing to two parts.
      size_t i = c->maxlen - c->tail;
      memcpy(buffer, get_ptr(c, c->tail), i * c->element_size);
      
      c->tail = 0;
      memcpy(&buffer[i*c->element_size], get_ptr(c, c->tail), next * c->element_size);
      c->tail = next;
      return size;
    }
}

size_t
ring_buffer_push_array (ring_buffer *c, uint8_t * buffer, size_t size)
{

  size_t capacity = ring_buffer_get_capacity(c);
  if (size > capacity)
    size = capacity;

  if (size == 0) 
    return size;
    
  uint32_t next = c->head + size; 

  // Check overflow for the head
  if (next >= c->maxlen )
   next -= c->maxlen;

  /**
   * c->head - is a start point and next - is an end pont. If next bigger than c->head - we
   * can write incoming buffer dirrectly to ringbuffer (without overflow). Otherwise we should
   * split writing to two parts: from head to maxlen (phisical end of rbuf) and from zero to next  
   */
  if (next >= c->head)
    {
      // Just write buffer as it is.
      memcpy(get_ptr(c, c->head), buffer, size * c->element_size);
      c->head = next;
      return size;
    } 
  else 
    {
      // Split writing to two parts.
      size_t i = c->maxlen - c->head;
      memcpy (get_ptr(c, c->head), buffer, i * c->element_size);
      
      c->head = 0;
      memcpy( get_ptr(c, c->head), &buffer[i * c->element_size], next * c->element_size);
      c->head = next;
      return size;
    }
}

size_t
ring_buffer_available (ring_buffer *c)
{
  return c->head >= c->tail ? c->head - c->tail : c->head + (c->maxlen - c->tail);
}

rbuf_status
ring_buffer_flush (ring_buffer *c)
{
  if (!c)
    return RBUF_ASSERT;
  c->tail = c->head;
  return RBUF_SUCCESS;
}

size_t
ring_buffer_get_capacity (ring_buffer *c)
{
  if (c->tail > c->head)
    {
      return c->tail - c->head - 1;
    }
  else
    {
      return c->maxlen - c->head + c->tail - 1;
    }
}

size_t
ring_buffer_get_size (ring_buffer *c)
{
  return c->maxlen - 1;
}
